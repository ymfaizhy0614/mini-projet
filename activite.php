<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Mini-projet-Activités</title>
	<meta charset="utf-8">
	<link rel="stylesheet"  href="style_general.css">
	<link href="jquery-ui-1.10.4.custom/css/base/jquery-ui-1.10.4.custom.css" rel="stylesheet">
	<script type="text/javascript" src="jquery-3.5.1.min.js"></script>
	<script src="jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
	<script src="jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.js"></script>

</head>
<body>
<form>
	
<!-- Activité 1  -->
	<h2>Activité 1 : Classe les salutations !</h2>
	<div id="A1">	
		<div id="depot1">
			<h3>Les situations formelles</h3>
			<img src="multimedia/formelle1.jpg" width="250" >
		</div>

		<div id="glisse">
			<p id="g1" class="infor" >Coucou !</p>
			<p id="g2" class="for" >Vous allez bien ?</p>
			<p id="g3" class="for" >Comment vous appelez-vous ?</p>
			<p id="g4" class="infor" >À toute !</p>
			<p id="g5" class="infor" >Ça va ? </p>
			<p id="g6" class="for" >Au plaisir de vous revoir ! </p>
		</div>

		<div id="depot2">
			<h3>Les situations informelles</h3>
			<img src="multimedia/informelle1.jpg" width="250">
		</div>
	</div>	


<!-- Activité 2  -->

    <h2> Activité 2 : Organise le dialogue !</h2>
    <div id="activite2">
	    <div id="dialogue">
	    	<p id="p5" class="phrase5" > Ouais, bye, à demain !</p>
	    	<p id="p3" class="phrase3" > Ça va </p>
	    	<p id="p7" class="phrase7" > Ah ! Oui c’est vrai, alors à lundi !</p>
	    	<p id="p1" class="phrase1" > Salut François, ça va ? </p>
	    	<p id="p6" class="phrase6" > Non, demain on est dimanche !</p>
	    	<p id="p2" class="phrase2" > Oui et toi, tu vas bien Caroline ? </p>
	    	<p id="p4" class="phrase4" > Bon, ben, tchao ! </p>
	    </div>
	    <div id="depotdialogue">
	    	<p id="d1" class="dep1" > Personne 1</p>
	    	<p id="d2" class="dep2" > Personne 2</p>
	    	<p id="d3" class="dep3" > Personne 1</p>
	    	<p id="d4" class="dep4" > Personne 2</p>
	    	<p id="d5" class="dep5" > Personne 1</p>
	    	<p id="d6" class="dep6" > Personne 2</p>
	    	<p id="d7" class="dep7" > Personne 1</p>
	    </div>
    </div>

<!-- Activité 3  -->
	<h2> Activité 3 : Est-il logique, le dialogue suivant ?</h2>
	<div id="A3">
		<div id="D1">
			<p>- Bonjour Louise !</p>
			<p>- Merci, madame</p>
			<input type="radio" name="R1" value="0" > Vrai
			<input type="radio" name="R1" value="1" > Faux
		</div>
		<div id="D2">
			<p>- Où habites-tu, Léo ?</p>
			<p>- Bonne soirée, monsieur !</p>
			<input type="radio" name="R2" value="0"> Vrai
			<input type="radio" name="R2" value="1"> Faux
		</div>
		<div id="D3">
			<p>- Salut ! Comment t'appelles-tu ?</p>
			<p>- Bonjour ! Je m'appelle Robert.</p>
			<input type="radio" name="R3" value="1"> Vrai
			<input type="radio" name="R3" value="0"> Faux
		</div>		
	</div> 
</form>

	<div id="boutons">
		<p id="message"></p>
		<input type="submit" value ="Valider" id="boutonV">
	</div>

	<script>
		//Activité 1
		$(function () {
			//pour la glisse 
			$(".for, .infor").draggable(
				{
					stop: function(event,ui) {
						$(this).hide();
					}
				}
			); 


			//pour le dépot 
			$("#depot1").droppable( 
				{
					accept : ".for",
					drop: function (event, ui) {
						 scoreA1final();
					}

				}
			); 

			$("#depot2").droppable(
				{
					accept : ".infor",
					drop: function (event, ui) {
						scoreA1final(); 
					}
				}
			); 

		}); 

		//pour le score Activité 1

		var scoreA1 = 0;

	    var scoreA1final = function() { 
	    	return ++scoreA1; 
	    };

	    //Activité 2
	    $(function () {
			//pour la glisse 
			$(".phrase5, .phrase3, .phrase7, .phrase1, .phrase6, .phrase2, .phrase4").draggable(
				{
					stop: function(event,ui) {
						return false; 
					}
				}
			); 

			//pour le dépot 
			$("#d1").droppable( 
				{
					accept : ".phrase1",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        }, 
				}
			); 

			$("#d2").droppable(
				{
					accept : ".phrase2",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        },	 
				}
			); 

			$("#d3").droppable( 
				{
					accept : ".phrase3",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        },		 
					}
			); 

			$("#d4").droppable( 
				{
					accept : ".phrase4",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        },		 
					}

			); 

			$("#d5").droppable( 
				{
					accept : ".phrase5",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        },		
				}
			); 

			$("#d6").droppable( 
				{
					accept : ".phrase6",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        },		
				}
			);

			$("#d7").droppable( 
				{
					accept : ".phrase7",
					drop: function (event, ui) {
						$(this).css ('background', 'rgb(0,200,0)');
						scoreA2final();
						},
                    over: function(event, ui) {
                        $(this).css('background', 'orange');
                        },		 

				}
			);  

		}); 

		//pour le score Activié 2 

		var scoreA2 = 0;  
	    var scoreA2final = function() { 
	    	return ++scoreA2; 
	    } 
	    
	    //pour le bouton Valider
	   
		$("#boutonV").on(
		"click", function(){

			// Le score d'activité 3 
			var item = $(":radio:checked");
			var len=item.length;
			if(len!=3){
			         alert('Il faut répondre à toutes les questions')
			         return false;
			       }				

			var scoreA3 =0;
			       $.each($(":radio:checked"),function(){
			         scoreA3+=parseInt(this.value);
			       })

			//traitement  de la question 
			let score1 = scoreA1; 
			let score2 = scoreA2;
			let score3 = scoreA3; 
			let scoreF = scoreA1 + scoreA2 + scoreA3; 
			let loginA = "<?php print $_SESSION['login']; ?>"; 
			donnees='score1=' + score1 + '&score2=' + score2 + '&score3='+ score3 + '&scoreF=' + scoreF +'&login='+ loginA; 

			$.ajax ({
				url:'mjscore.php', //Notre page de score
				type: 'POST', //La requête
				data: donnees,
				dataType:'html',
				beforeSend : function(){
					$("#boutonV").attr({disabled:"disabled"});
				},
				success : function(reponse,statut) {
			    $("#message").html(reponse);
			    },
			    complete :function(){
			    	$("#boutonV").removeAttr({disabled:"disabled"});
			    }
			})
		});
	</script>

</body>
</html>