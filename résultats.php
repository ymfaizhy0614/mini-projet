<!DOCTYPE html>
<html>
<head>
	<title>Mini-projet-Résultats</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css"  href="style_general.css"/>
	<script type="text/javascript" src="jquery-3.5.1.min.js"></script>
	<script src="jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
	<script src="jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.js"></script>
</head>
<body>
<div id="resultat">
	<table> 
			<caption> Score des activités</caption>

			<tr>
				<th>Nom</th>
				<th>Prénom</th>
				<th>Activité 1</th>
				<th>Activité 2</th>
				<th>Activité 3</th>
				<th>Score final</th>
			</tr>
		<?php
			
			include_once("connexion.php"); // connexion à la BD
	   

			$requete = "SELECT nom, prenom, a1, a2, a3, scorefinal FROM utilisateurs WHERE status='apprenant'";//
			$reponse = $pdo->query($requete);
			while ($res = $reponse->fetch()) {

		?>
		<tr>
			<td><?php echo $res['nom']; ?></td>
			<td><?php echo $res['prenom']; ?></td>
			<td><?php echo $res['a1']; ?></td>
			<td><?php echo $res['a2']; ?></td>
			<td><?php echo $res['a3']; ?></td>
			<td><?php echo $res['scorefinal']; ?></td> 
		</tr>

		<?php
			}
			$reponse->closeCursor(); 	
		?>
	</table>	
</div>
	
</body>
</html>