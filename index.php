<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title> Connexion au site </title>
		<link rel="stylesheet" href="style_general.css">
		<script type="text/javascript" src="jquery-3.5.1.min.js"></script>
		<script src="jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
		<script src="jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.js"></script>
	</head>
	<body>
	    <div id="connexion">

		<!-- formulaire d'identification -->
			<form method="POST" action="index.php">
				<h1> Les activités de Salutation </h1>
				<label class="labelInfo">Login :</label> <input type="text" name="login" class="texte" /><br/>
				<label class="labelInfo">Mot de passe :</label> <input type="password" name="mdp" class="texte"/><br/>
				<input id="boutonC" type="submit" value="se connecter" name="ok"/>
			</form>
	    	
	    </div>
		<?php   
			/* Version synchrone (rechargement de la page)     */
			
			if (isset($_POST['ok'])) { // le bouton submit a été cliqué
				// récupération des variables passées en POST

				$login = $_POST['login'];
				$_SESSION['login']= $login; 
				$mdp = $_POST['mdp'];
                //$status = $_POST['status'];
				if ($login =="" || $mdp =="" ) {
					print '<Script Language ="JavaScript"> alert ("Attention, Login/ Mot de passe vide,")</script>';
				}else {
					include_once("connexion.php"); // connexion à la BD
					
					$requete = "SELECT nom, prenom, mdp, status FROM utilisateurs WHERE login='$login'";
					$reponse = $pdo->query($requete); // exécution de la requête
					if ($enr = $reponse->fetch()) { // la requête renvoie une réponse
						if ($mdp==$enr['mdp'] & $enr['status']=='apprenant') { // le mot de passe et le status apprenant est correct
							$nom = $enr['nom']; 
							$prenom = $enr['prenom'];
							echo'<html><head><Script Language="JavaScript">alert("Bienvenu(e) '.$prenom.' !");</Script></head></html>' . "<meta http-equiv=\"refresh\" content=\"0;url=activite.php\">"; 
					    }elseif ($mdp==$enr['mdp'] & $enr['status']=='enseignant') {// le mot de passe et le status enseignant  est correct
					    	$nom = $enr['nom'];
							$prenom = $enr['prenom'];
							echo'<html><head><Script Language="JavaScript">alert("Bienvenu(e) '.$prenom.' !");</Script></head></html>' . "<meta http-equiv=\"refresh\" content=\"0;url=résultats.php\">"; 
						} else { // le mot de passe est incorrect
							print '<Script Language ="JavaScript"> alert ("Attetion, Mot de passe incorrect")</script>';
						}
					} else { // la requête ne renvoie pas de réponse
						print '<Script Language ="JavaScript"> alert ("Attention, Login incorrect,")</script>';
					}					
				}

			}

		?>
	</body>
</html>